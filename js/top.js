		/**
		 * 日期时间、温度 获取并设置
		 */
		if(!window.sessionStorage.temp_curr) {
			$.ajax({
				type: "GET",
				url: window.sessionStorage.server_url + "/api/junctioncore/v1/page/search/",
				data: {
					hotel_id: window.localStorage.hotelId,
					restaurant_id: window.localStorage.restaurantId,
					device_id: window.localStorage.deviceId,
					device_type: "tv",
					page_type: "info_page",
					lang_agnostic_id: "local_weather"
				},
				success: function(data) {
					window.sessionStorage.temp_curr = data.data.weather_data.today.temp_curr;
					$("#wendu").html(data.data.weather_data.today.temp_curr + "℃");
				}
			});
		} else {
			$("#wendu").html(window.sessionStorage.temp_curr + "℃");
		}
		function dateweek() {	
			var date = new Date();
			var seperator = "/";
			var month = date.getMonth() + 1;
			var strDate = date.getDate(parseInt(window.sessionStorage.localTime));
			var week = window.sessionStorage.language == "zh-cn" ? ['周日', '周一', '周二', '周三', '周四', '周五', '周六'][date.getDay()] : ['Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'][date.getDay()];
			if(month >= 1 && month <= 9) {
				month = "0" + month;
			}
			if(strDate >= 0 && strDate <= 9) {
				strDate = "0" + strDate;
			}
			var currentdate = month + seperator + strDate +
				" " + week;
			return currentdate;
		}
		var t = null;
		t = setTimeout(time(), 1000); //开始执行
		function time() {
			clearTimeout(t); //清除定时器
			if(!window.sessionStorage.localTime || isNaN(window.sessionStorage.localTime)) {
				$.ajax({
					type: "GET",
					url: window.sessionStorage.server_url + "/api/junctioncore/v1/util/now/",
					beforeSend: function(data) {
						data.setRequestHeader("CONTENT-TYPE", "application/x-www-form-urlencoded");
					},
					dataType: "json",
					data: {},
					success: function(data) {
						//转换服务器时间格式2018-5-28 13:00 转为： 2018/5/28 13:00
						//前者不兼容除chrome核心外的其他浏览器。
						window.sessionStorage.localTime = Date.parse(new Date((data.data.datetime).split("-").join("/")));
						setTime(window.sessionStorage.localTime);
						t = setTimeout(time, 1000); //设定定时器，循环执行
					}
				});
			} else {
				//  	$("#timemm").html("err")
				//  	$("#timemm").html(window.sessionStorage.localTime)
				window.sessionStorage.localTime = parseInt(window.sessionStorage.localTime) + 1000;
				setTime(window.sessionStorage.localTime);
				t = setTimeout(time, 1000); //设定定时器，循环执行
			}
		};
	
		function setTime(dateLong) {
			var date = new Date();
			date.setTime(dateLong);
			var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
			var minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
			$("#timemm").html(hour + ":" + minute);
		}
		var dateweek = dateweek();